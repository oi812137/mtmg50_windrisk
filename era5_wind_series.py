"""
Template code for looping over dates, reading multiple ERA5 data files in netCDF format,
sub-setting the data from a particular location and creating a time series.

Author: Based on code by John Methven (2020), additional code by 29812137

CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

import matplotlib.pyplot as plt
import numpy as np
from netCDF4 import Dataset
import datetime
from datetime import date
from datetime import timedelta
from init import *

##############################################################################
#                           RAW DATA FILE PROCESSING                         #
##############################################################################

def extract_series(fpath, fstem, lonpick, latpick, dstart, dend):
    '''
    High level function controlling extraction of runoff time series 
    for chosen location.
    Input: fpath, fstem determine the name of file to read
    :lonpick    - longitude of chosen location
    :latpick    - latitude of chosen location
    :dstart     - start date in datetime.date format
    :dend       - end date in datetime.date format
    Output: 
    :windarr    - wind speed (m/s) time series at chosen location
    date_dates  - array of dates of time series
    '''   

    # Set end date and start date of required time series
    dendp = dend+timedelta(days=1)
    tinterval = dendp-dstart
    ndays = tinterval.days

    # Plot the data for the first date in the interval
    fdate = dstart.strftime("%Y_%m")
    fyear = dstart.year
    iprint = 0  # set to 1 to print variables on reading files; 0 for no print
    
    #Read the data
    filename = str(fpath+fstem+fdate+'_DET.nc')
    alon, alat, outdates, u, v, mslp = read_era(filename, fyear, iprint)

    # Find the indices of the grid box centred closest to the chosen location
    intlon, intlat = subset_field(alon, alat, lonpick, latpick, iprint)

    # Setup arrays to save time series data
    windarr = np.zeros(ndays)

    # Loop over months, reading files and saving daily data
    icarryon = 1
    dcur = dstart
    n=0
    
    data_dates = []
    
    while icarryon == 1:
        fdate = dcur.strftime("%Y_%m")
        fyear = dcur.year
        
        #Read the data
        filename = str(fpath+fstem+fdate+'_DET.nc')
        
        # Note that the str() function is included to ensure that these
        # variables are interpreted as character strings.
        alon, alat, outdates, u, v, mslp = read_era(filename, fyear, iprint)
        n_days_to_process = len(outdates)
        
        # Check whether the last day requested is within this month
        if n+n_days_to_process >= ndays:
            n_days_to_process = ndays-n
            icarryon = 0
        
        # Save the daily data for given location for this month
        for i in range(n_days_to_process):
            data_dates.append(dcur + timedelta(days=i))
            windarr[n+i] = np.sqrt(u[i, intlat, intlon]**2 + v[i, intlat, intlon]**2)
        
        # Increment the date variables by number of days processed        
        n = n+n_days_to_process
        dcur=dcur+timedelta(days=n_days_to_process)
    
    return windarr, np.array(data_dates)

def extract_multi_series(fpath, fstem, dstart, dend, loadDataFromFile):
       
    if loadDataFromFile:
        data_dates = np.load('ERA5_DATES.npy', allow_pickle=True)
        alat = np.load('ERA5_LAT.npy')
        alon = np.load('ERA5_LON.npy')
        windarrs = np.load('ERA5_WIND.npy', allow_pickle=True)
    else:            
        # Load and process raw data
    
        # Set end date and start date of required time series
        dendp = dend+timedelta(days=1)
        tinterval = dendp-dstart
        ndays = tinterval.days
        
        fdate = dstart.strftime("%Y_%m")
        fyear = dstart.year
        iprint = 0  # set to 1 to print variables on reading files; 0 for no print
        #Read the data
        filename = str(fpath+fstem+fdate+'_DET.nc')
    
        alon, alat, outdates, u, v, mslp = read_era(filename, fyear, iprint)
                
        # Create meshgrid where [i,j] component is the ith longitude and jth latitude
        lats, lons = np.meshgrid(alat, alon)
        
        # Grid to store wind time series
        windarrs = np.empty_like(lats, dtype=object)
        for i in range(len(alon)):
            for j in range(len(alat)):
                windarrs[i,j]=[]
    
        # Loop over months, reading files and saving daily data for each gridbox
        icarryon = 1
        dcur = dstart
        data_dates = []
        n=0
        while icarryon == 1:
            fdate = dcur.strftime("%Y_%m")
            fyear = dcur.year
            # Read the monthly data
            filename = str(fpath+fstem+fdate+'_DET.nc')
            alon, alat, outdates, u, v, mslp = read_era(filename, fyear, iprint)
    
            n_days_to_process = len(outdates)
            
            # Check whether the last day requested is within this month
            if n + n_days_to_process >= ndays:
                n_days_to_process = ndays-n
                icarryon = 0
    
            # Save the daily winter data for this month for each gridbox
            # if dcur.month in (12,1,2):
            for k in range(n_days_to_process):
                # Store array of dates 
                data_dates.append(dcur + timedelta(days=k))
                for i in range(len(alon)):
                    for j in range(len(alat)):                        
                        windarrs[i,j].append(np.sqrt(u[k, j, i]**2 + v[k, j, i]**2))
    
            # Increment date variables by number of days processed
            n = n+n_days_to_process
            dcur=dcur+timedelta(days=n_days_to_process)
            
        for i in range(len(alon)):
            for j in range(len(alat)):                        
                windarrs[i,j] = np.array(windarrs[i,j])
            
        np.save('ERA5_DATES', np.array(data_dates))
        np.save('ERA5_LAT', alat)
        np.save('ERA5_LON', alon)
        np.save('ERA5_WIND', windarrs)
        
    return alat, alon, windarrs, data_dates

def read_era(filename, fyear, iprint):

    '''
    Read in the ground variable data from the netCDF file.
    Input: name of file to read.
    Output: 
    :longitude  - degrees
    :latitude   - degrees
    :outdates   - calendar dates for data points
    :u          - zonal component of the wind (m/s)
    :v          - meridional component of the wind (m/s)
    :mslp       - mean sea level pressure (hPa)
    '''
    if iprint == 1:
        print()
        print('Reading file ',filename)
    data = Dataset(filename, 'r')

    if iprint == 1:
        print(data)
        print()
        print(data.dimensions)
        print()
        print(data.variables)
        print()
        
    ftime = data.variables['time'][:]
    alon = np.array(data.variables['lon'][:])
    alat = np.array(data.variables['lat'][:])
    u = data.variables['u10'][:,:,:]
    v = data.variables['v10'][:,:,:]
    # Note that pressure is converted from Pa to hPa
    # In some years the files contain mean sea level pressure.
    # In some years the files contain surface pressure.
    if any((fyear < 2000, fyear >= 2018)):
        mslp = 0.01*data.variables['msl'][:,:,:]
    else:
        mslp = 0.01*data.variables['sp'][:,:,:]

    data.close()
    #
    # Time is in hours since 00UT, 1 Jan 1900.
    # Convert to timedelta format and then add to startcal to make a list of dates.
    # Note that you can add times in datetime and timedelta formats
    # which allows for leap years etc in time calculations.
    #
    startcal = datetime.datetime(1900, 1, 1)
    outdates = [startcal+timedelta(hours=ftimel) for ftimel in ftime]

    return alon, alat, outdates, u, v, mslp


def subset_field(alon, alat, lonpick, latpick, iprint):

    '''
    Find the indices of the grid point centred closest to chosen location.
    Input: 
    :alon       - longitude points
    :alat       - latitude points
    :lonpick    - longitude of chosen location
    :latpick    - latitude of chosen location
    Output:
    :intlon     - index of longitude for chosen point
    :intlat     = index of latitude for chosen point
    '''
    #
    # Using the fact that longitude and latitude are regularly spaced on grid.
    # Also, points ordered from north to south in latitude.
    # The indices (intlon, intlat) correspond to the centre of the closest grid-box 
    # to the chosen location.
    #
    dlon = alon[1]-alon[0]
    dlat = alat[1]-alat[0] # note that dlat is negative due to ordering of grid
    lonwest = alon[0]-0.5*dlon
    latnorth = alat[0]-0.5*dlat
    intlon = int(round((lonpick-lonwest)/dlon))
    intlat = int(round((latpick-latnorth)/dlat))
    
    if iprint==1:
        print()
        print('Longitude of nearest grid box = ',alon[intlon])
        print('Latitude of nearest grid box = ',alat[intlat])
    
    return intlon, intlat