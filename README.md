# MTMG50_WindRisk

Download all files in parent directory to a single folder.

To execute code against pre-processed data files (recommended):

1. Create a sub-directory called "Data"

2. Download files from "Data" directory to this location

3. Open and execute main.py

To execute code against raw data files (slower):

1. Create a sub-directory called "Data"

2. Under "Data" create subfolders "ECMWF", "ERA5", "NCEP" and copy raw data files here

3. In main.py, change loadDataFromFile value in line 28 from True to False

4. In main.py, change loadDataFromFile value in line 37 from True to False

5. Open and execute main.py

CODE STRUCTURE

main.py: Single entry point for full execution

taskWrapper.py: Task-based high level function calls

era5_wind_series.py: Code for loading and processing ERA5 data

S2S_hincast.py: Code for loading and processing S2S hindcast data

init.py: variable initialisation

utilities.py: data manipulation toolkit

plotter.py: matplotlib plotting logic

cartopy.py: cartopy plotting logic
