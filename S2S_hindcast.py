"""
Author: 2022, 29812137
              Based on code by John Methven
              
              CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

# import matplotlib.pyplot as plt
#import cartopy.crs as ccrs
import numpy as np
from netCDF4 import Dataset
import datetime
import utilities as utl
# from datetime import date
from datetime import timedelta
from init import *

def get_alp_probability(intlon, intlat, alp, windarrs_ecmwf, wind_98s_all_months):
    """
    Calculate the probability of a given day having an ALP greater than that
    passed in. Calculate ALP at the given location for all dates, all ensembles,
    lead times BETWEEN 0 AND 5. Return probability > alp
    
    Parameters
    ==========
    intlon: index of lonitude of location
    intlat: index of latitude of location
    alp: alp to check for values greater than
    windarrs_ecmwf: 2d array of 3d arrays of wind speeds 
    wind_98s_all_months: V98 figures for each gridbox 
    
    Returns
    =======
    Probability (scaler)
    """

    total_count = 0
    alp_count = 0

    w_shape = windarrs_ecmwf[0,0].shape
    
    # Pull out wind data for this grid point
    windarr = windarrs_ecmwf[intlon,intlat]
    
    # Iterate through all available dates
    for k in range(w_shape[1]):
        # Iterate through lead times 0 to 5
        for ld in range(6):
            # Iterate through ensembles
            for e in range(w_shape[0]):
                total_count += 1
                windSpeed = windarr[e,k,ld]
                SSI = utl.calculate_SSI(windSpeed, wind_98s_all_months[intlon,intlat])
                if SSI > alp:
                    alp_count += 1
                    
    return alp_count/total_count
    

def three_day_alp_distribution(storm_peak_date, alon, alat, windarrs_ecmwf, \
                               file_dates_ecmwf, lead_days, wind_98s_all_months):
    """
    Calculate the distribution of accumlated loss potential across Europe over 
    the three days centred on storm_peak_date, lead_time is the number of days 
    to the first of the three days
    
    Parameters
    ----------
    storm_peak_date : date of Umax as per observational data
    alat, alon: np arrays of latitude and longitude
    windarrs_ecmwf : 2d array [lon,lat] of 5d arrays [ensemble idx,file date idx,lead time idx]
    file_dates_ecmwf: array of dates corresponding to windarrs_ecmwf data
    lead_days : number of days from the S2S hindcast date to the first of the 
                three days in consideration
    wind_98s_all_months : 98th percentile wind speeds over a long historic period
    
    Returns
    =======
    Nparray with ine time series of winds for each ensemble
    """
    
    # We want data from the forecast for (storm_peak_date - lead_days days - 1 day)
    # for lead time "lead_time", "leda_time+1", "leda_time+2"
    file_date = storm_peak_date - timedelta(days=(lead_days+1))
    file_date_idx = np.where(file_dates_ecmwf==file_date)[0][0]
    
    # Retrieve wind data for each ensemble for each of the three dates
    
    # Fetch shape of wind data series [ensemble idx,file date idx,lead time idx]
    w_shape = windarrs_ecmwf[0,0].shape
    
    num_ensembles = w_shape[0]
    num_days = 3
    
    # List of 3 x 2d [lon,lat] arrays to store wind data for the three days in question
    # List of 3 x 2d [lon,lat] arrays to SSI data for the three days in question
    
    ALPs = np.zeros(shape=(num_ensembles))

    for n in range(num_days):
        
        # Process each grid point, storing "e" wind values for each grid point
        # Extract data from file with date "data_date" and lead times "lead_days",
        # "lead_days+1", lead_days + 2"
        # Calculate SSIs
        for i in range(len(alon)):
            for j in range(len(alat)):
                # Pull out wind data for this grid point
                windarr = windarrs_ecmwf[i,j]
                for e in range(num_ensembles):
                    # Wind speed for this grid point, ensemble, file date, lead time
                    windSpeed = windarr[e,file_date_idx,lead_days+n]

                    # Sum total SSI across Europe across all three days
                    SSI = utl.calculate_SSI(windSpeed, wind_98s_all_months[i,j])

                    ALPs[e] += SSI
                    
                    # print(f'i {i} j {j} e {e} w {windSpeed} 98th: {wind_98s_all_months[i,j]} SSI {SSI}')
                    
    return ALPs

def ninety_eighth_percentile_by_lead_time(\
                              windarr_london, windarr_paris, windarr_hamburg):
    """    
    Parameters
    ----------
    windarr_london : Array of wind speeds: [ensemble idx,file date idx,lead time idx]
    windarr_paris : Array of wind speeds: [ensemble idx,file date idx,lead time idx]
    windarr_hamburg : Array of wind speeds: [ensemble idx,file date idx,lead time idx]

    Returns
    -------
    _98th_perc_london:  Array of 98th percentiles by lead time: [lead time idx]
    _98th_perc_paris:   Array of 98th percentiles by lead time: [lead time idx]
    _98th_perc_hamburg: Array of 98th percentiles by lead time: [lead time idx]
    """
    
    # Shape is (ensemble idx,file date idx,lead time idx)
    wnd_shape = windarr_london.shape
    
    _98th_perc_london = np.zeros(shape=(wnd_shape[2]))
    _98th_perc_paris = np.zeros(shape=(wnd_shape[2]))
    _98th_perc_hamburg = np.zeros(shape=(wnd_shape[2]))
    
    for l in range(wnd_shape[2]):
        _98th_perc_london[l] = np.percentile(windarr_london[:,:,l], 98)
        _98th_perc_paris[l] = np.percentile(windarr_paris[:,:,l], 98)
        _98th_perc_hamburg[l] = np.percentile(windarr_hamburg[:,:,l], 98)
    
    return _98th_perc_london, _98th_perc_paris, _98th_perc_hamburg

def ninety_eighth_percentile_by_elt(\
                                windarr_london, windarr_paris, windarr_hamburg):
    """    
    Parameters
    ----------
    windarr_london : Array of wind speeds: [ensemble idx,file date idx,lead time idx]
    windarr_paris : Array of wind speeds: [ensemble idx,file date idx,lead time idx]
    windarr_hamburg : Array of wind speeds: [ensemble idx,file date idx,lead time idx]

    Returns
    -------
    _98th_perc_london:  Array of 98th percentiles by ensemble and lead time: 
                        [lead time idx]
    _98th_perc_paris:   Array of 98th percentiles by ensemble and lead time: 
                        [lead time idx]
    _98th_perc_hamburg: Array of 98th percentiles by ensemble and lead time: 
                        [lead time idx]
    """

    # Shape is (ensemble idx,file date idx,lead time idx)
    wnd_shape = windarr_london.shape
    
    _98th_perc_london_elt = np.zeros(shape=(wnd_shape[0], wnd_shape[2]))
    _98th_perc_paris_elt = np.zeros(shape=(wnd_shape[0], wnd_shape[2]))
    _98th_perc_hamburg_elt = np.zeros(shape=(wnd_shape[0], wnd_shape[2]))
    
    for e in range(wnd_shape[0]):
        for l in range(wnd_shape[2]):
            _98th_perc_london_elt[e,l] = np.percentile(windarr_london[e,:,l], 98)
            _98th_perc_paris_elt[e,l] = np.percentile(windarr_paris[e,:,l], 98)
            _98th_perc_hamburg_elt[e,l] = np.percentile(windarr_hamburg[e,:,l], 98)
    
    return _98th_perc_london_elt, _98th_perc_paris_elt, _98th_perc_hamburg_elt
    
def get_single_e_lta(windarrs_ecmwf):
    """
    windarrs_ecmwf 2d array [lon,lat] of 5d arrays [ensemble idx,file date idx,lead time idx]
    """
    w_shape = windarrs_ecmwf.shape
    y_shape = windarrs_ecmwf[0,0].shape
    windarrs_filtered = np.empty(shape=w_shape, dtype=object)
    
    for i in range(w_shape[0]):
        for j in range(w_shape[1]):
            windarrs_filtered[i,j]=[]
    
    for i in range(w_shape[0]):
        for j in range(w_shape[1]):
            windarrs_filtered[i,j]=[]
            for k in range(y_shape[1]):
                windarrs_filtered[i,j].append(windarrs_ecmwf[i,j][0,k,1])
            windarrs_filtered[i,j]=np.array(windarrs_filtered[i,j])
    
    return windarrs_filtered

##############################################################################
#                           RAW DATA FILE PROCESSING                         #
##############################################################################

def extract_multi_series(fpath, fstem, file_dates, iprint):
 
    # Read the first data file and gather metadata
    fdate = file_dates[0].strftime("%Y%m%d")
    filename = str(fpath+fstem+fdate+'.nc')
    alon, alat, aensemble_member, forecast_dates, u, v = read_hc(filename, iprint)
  
    # Create meshgrid where [i,j] component is the ith longitude and jth latitude    
    
    lats, lons = np.meshgrid(alat, alon)

    # Create grid to store wind time series for each grid point
    windarrs = np.empty_like(lats, dtype=object)
    
    # Create containers for wind time series
    for i in range(len(alon)):
        for j in range(len(alat)):
            # At each grid point we will have a 3D array indexed by:
            # [ensemble member index, file date index, lead time index]
            windarrs[i,j]=np.zeros(shape=(len(aensemble_member),len(file_dates),\
                                          len(forecast_dates)))
    
    # Process each requested file date
    for fd in range(len(file_dates)):
        
        print('Processing ' + str(file_dates[fd]) + ', ' + str(datetime.datetime.now()))
        
        # Read the daily data file
        fdate = file_dates[fd].strftime("%Y%m%d")
        filename = str(fpath+fstem+fdate+'.nc')
        
        # u indexed by [lead time index, ensemble member index, lat, lon]
        alon, alat, ensemble_members, forecast_dates, u, v  = read_hc(filename, iprint)

        # Save the daily data required from this file for each gridbox
        for i in range(len(alon)):
            for j in range(len(alat)):
                for e in range(len(ensemble_members)):
                    for l in range(len(forecast_dates)):
                        windarrs[i,j][e,fd,l] = np.sqrt(u[l,e,j,i]**2 + v[l,e,j,i]**2)

    # alat, alon, windarrs are in sync
    # windarrs[0,0] is [lon,lat] = [-12,60] - TOP LEFT CORNER OF MAP
    # Flip latitude indices to be in line with ERA5 logic
    
    # Latitude is are indexed opposite to ERA5
    # Invert latitude indexing for consistency
    alat_flipped = np.flip(alat)
    windarrs_flipped = np.flip(windarrs, 1)
    
    return alat_flipped, alon, windarrs_flipped

def read_hc(filename, iprint):

    '''
    Read in the ground variable data from the netCDF file.
    Input: name of file to read.
    Output: 
    :longitude  - degrees
    :latitude   - degrees
    :outdates   - calendar dates for data points
    :u          - zonal component of the wind (m/s)
    :v          - meridional component of the wind (m/s)
    :mslp       - mean sea level pressure (hPa)
    '''
    if iprint == 1:
        print()
        print('Reading file ',filename)
    data = Dataset(filename, 'r')

    if iprint == 1:
        print(data)
        print()
        print(data.dimensions)
        print()
        print(data.variables)
        print()
        
    forecast_dates = data.variables['time'][:]         # 45
    ensemble_members = data.variables['number'][:]     # 4 
    alat = np.array(data.variables['latitude'][:])     # 15 
    alon = np.array(data.variables['longitude'][:])    # 23   

    # u and v indexed by lead time index, ensemble member index, lat, lon
    u = data.variables['u10'][:,:,:,:]
    v = data.variables['v10'][:,:,:,:]

    data.close()
    
    return alon, alat, ensemble_members, forecast_dates, u, v
    
def get_hc_data(datafilestem, fpath, fstem, dstart, dend, loadDataFromFile):
    
    if datafilestem == 'ECMWF':
        file_dates = utl.get_ecmwf_days(dstart, dend)
    else:
        file_dates = utl.get_winter_days(dstart, dend, include29thFeb=False)
    
    if loadDataFromFile:
        alat = np.load(datafilestem + '_S2S_LAT.npy')
        alon = np.load(datafilestem + '_S2S_LON.npy')
        windarrs = np.load(datafilestem + '_S2S_WIND.npy', allow_pickle=True)        
    else:            
        # Load and process raw data                
        alat, alon, windarrs = extract_multi_series(\
                                           fpath, fstem, file_dates, iprint=0)
        
        np.save(datafilestem + '_S2S_LAT', alat)
        np.save(datafilestem + '_S2S_LON', alon)
        np.save(datafilestem + '_S2S_WIND', windarrs)
      
    # alat, alon, windarrs are in sync
    # WARNING - windarrs[0,0] is [lon,lat] = [-12,60 - TOP LEFT CORNER OF MAP
    return alat, alon, windarrs, file_dates