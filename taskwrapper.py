#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 18:55:43 2022

@author: 29812137

CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

from init import *
import datetime as dt
from datetime import timedelta

import plotter as plt
import numpy as np
import utilities as utl

import era5_wind_series as rws
import readera_ground_cartopy as rgc
import S2S_hindcast as hc

def Question1(dstart, dend, winterFilter, boxplotlimit,filestem):
    """
    Plot wind distribution, all AND winter months, at London, Paris & Hamburg (ERA5)

    Parameters
    ==========
    dstart: start of date range
    dend: end of date range
    winterFilter: boolean flag to indicate whether to filter to only winter months
    boxplotlimit: plotting paremeter (y axis limit)
    filestem: stem of filename for image
    
    Returns
    =======
    None
    
    """
    
    # London - fetch wind data
    windarr_london, data_dates = rws.extract_series(\
       FPATH_DATA, FSTEM_DATA, LONDON_LON, LONDON_LAT, dstart, dend)
        
    # Paris - fetch wind data
    windarr_paris, data_dates = rws.extract_series(\
       FPATH_DATA, FSTEM_DATA, PARIS_LON, PARIS_LAT, dstart, dend)
    
    # Hamburg - fetch wind data
    windarr_hamburg, data_dates = rws.extract_series(\
       FPATH_DATA, FSTEM_DATA, HAMBURG_LON, HAMBURG_LAT, dstart, dend)
                
    if winterFilter:        
        print('Question 1 - winter months')
        #Filter out non-winter months
        windarr_london_wnt = utl.filter_winter(windarr_london, data_dates)
        windarr_paris_wnt = utl.filter_winter(windarr_paris, data_dates)
        windarr_hamburg_wnt = utl.filter_winter(windarr_hamburg, data_dates)  
        
        title = 'Wind Speed, Dec 1990-Feb 2010, Winter Months (ERA5)'
        plt.PlotQuestion1(windarr_london, windarr_paris, windarr_hamburg, \
                          data_dates, title, boxplotlimit, filestem)
    else:
        print('Question 1 - all months')
        title = 'Wind Speed, Jan 1979-Feb 2010, All Months (ERA5)'
        plt.PlotQuestion1(windarr_london, windarr_paris, windarr_hamburg, \
                          data_dates, title, boxplotlimit, filestem)

    
def Question2(dstart, dend, loadDataFromFile = False):
    """
    Plot 98th percentile, all months AND winter months, across Europe (ERA5)
    
    Parameters
    ==========
    dstart: start of date range
    dend: end of date range
    loadDataFromFile: boolean to indicate whether to load data from pre-processed
                      numpy data files (True) or from raw data files (False)
                      
    Returns
    =======
    alat: nparray of latitudes (from ERA5 data)
    alon: nparray of longitudes (from ERA5 data)
    windarrs_winter: 2d array of winter wind time series (at each location)
    data_dates_winter: array of data dates corresponding to windarrs_winter time series
    wind_98s_all_months_map: V98 figures for each gridbox (with matrix rotated
                             90 degrees clockwise for correct cartopy plotting)
    wind_98s_winter_months: V98 figures calculated over only winter months    
    """
    
    # Extract wind data for Europe (ERA5) - ALL MONTHS
    # windarrs_all_months = (23,15)
    alat, alon, windarrs_all_months, data_dates = rws.extract_multi_series(\
                        FPATH_DATA, FSTEM_DATA, dstart, dend, loadDataFromFile)
        
    # Calculate 98th percentiles of wind across Europe - ALL MONTHS
    # wind_98s_all_months = (15, 23)
    wind_98s_all_months_map = utl.calculate_percentile(windarrs_all_months, 98)
    
    # Plot 98th percentiles on map - ALL MONTHS
    rgc.plot_onproj2d(alon, alat, wind_98s_all_months_map,\
      '98th Pcntl Wind (m/s), Jan 1979-Feb 2010, All Months (ERA5)','OrRd',\
          'Question2AllMonths', vmin=0,vmax=22)
    
    # Filter wind data to only include winter months 
    # windarrs_winter = (23,15)
    windarrs_winter, data_dates_winter = utl.filter_wind2d_winter(\
                                               windarrs_all_months, data_dates)
    
    # Calculate 98th percentiles of wind across Europe - WINTER MONTHS
    # wind_98s_winter_months = (15, 23)
    wind_98s_winter_months = utl.calculate_percentile(windarrs_winter, 98)
    
    # Plot 98th percentiles on map - WINTER MONTHS
    rgc.plot_onproj2d(alon, alat, wind_98s_winter_months,\
      '98th Pcntl Wind (m/s), Jan 1979-Feb 2010, Winter Months (ERA5)','OrRd',\
          'Question2WinterMonths', vmin=0,vmax=22)
    
    return alat, alon, windarrs_winter, data_dates_winter, wind_98s_all_months_map\
        , wind_98s_winter_months

def Question3(alat, alon, windarrs_winter, data_dates_winter, dstart, dend, \
              wind_98s_all_months_map):
    """
    Plot accumulated loss potential in winter months across Europe (ERA5)
    filtering data on dstart / dend
    
    Parameters
    ==========
    alat: nparray of latitudes
    alon: nparray of longitudes
    windarrs_winter: 2d array of winter wind time series (at each location)
    data_dates_winter: array of data dates corresponding to windarrs_winter time series
    dstart: start of date range
    dend: end of date range
    wind_98s_all_months_map: V98 figures for each gridbox ((with matrix rotated
                             90 degrees clockwise for correct cartopy plotting)
    
    Returns
    =======
    None
    
    """    
    
    # Filter (winter) wind time series on dates provided
    windarrs_filtered, data_dates_filtered  = \
        utl.filter_wind2d_by_date_range(windarrs_winter, \
             data_dates_winter, dstart, dend)
    
    # Calculate SSIs from data passed in
    SSIs = utl.calculate_SSIs(windarrs_filtered, wind_98s_all_months_map)

    rgc.plot_onproj2d(alon, alat, SSIs,'ALP, Dec 1999-Feb 2010, Winter Months (ERA5)',\
                      'rainbow', 'Question3WinterMonths')

def Question4(dstart, dend, loadDataFromFile = True):
    """
    Plot wind distribution in winter months at London, Paris & Hamburg (S2S)
    Load data from either raw files (loadDataFromFile = False) or numpy files
    (loadDataFromFile = True). If loaded from raw files, save to numpy files
    Plot and return S2S data
    
    Parameters
    ==========
    dstart: start of date range
    dend: end of date range
    loadDataFromFile: boolean to indicate whether to load data from pre-processed
                      numpy data files (True) or from raw data files (False)
                      
    Returns
    =======
    windarrs_ecmwf: 2d nparray of 3d nparrays
                    2d: [longitude (idx), latitude (idx)]
                    3d: [ensemble (idx), file date(idx), lead time(idx)]
                    Each member of the 3d nparray has a wind speed value
    file_dates_ecmwf: 1d array of file dates 
    """    

    # # NCEP
    alat_ncep, alon_ncep, windarrs_ncep, file_dates_ncep = hc.get_hc_data(\
        'NCEP', FPATH_NCEP, FSTEM_HC, dstart, dend, loadDataFromFile)   
     
    intlon_london, intlat_london = rws.subset_field(alon_ncep, alat_ncep, \
                                                    LONDON_LON, LONDON_LAT, iprint=0)
    intlon_hamburg, intlat_hamburg = rws.subset_field(alon_ncep, alat_ncep, \
                                                      HAMBURG_LON, HAMBURG_LAT, iprint=0)
    intlon_paris, intlat_paris = rws.subset_field(alon_ncep, alat_ncep, PARIS_LON, \
                                                  PARIS_LAT, iprint=0)
    
    print('Question 4 - NCEP')
    plt.PlotQuestion4NCEP(windarrs_ncep[intlon_london,intlat_london][:,:,:],\
                          windarrs_ncep[intlon_hamburg,intlat_hamburg][:,:,:],\
                          windarrs_ncep[intlon_paris,intlat_paris][:,:,:],\
                          'Question4NCEP')
        
    # Load ECMWF data
    
    print('Question 4 - ECMWF')
    
    alat_ecmwf, alon_ecmwf, windarrs_ecmwf, file_dates_ecmwf = hc.get_hc_data(\
        'ECMWF', FPATH_ECMWF, FSTEM_HC, dstart, dend, loadDataFromFile) 
        
    # Plot wind speed histograms across entire dataset for London, Paris, Hamburg (ECMWF)

    plt.PlotQuestion4ECMWF(windarrs_ecmwf[intlon_london,intlat_london][:,:,:],\
                            windarrs_ecmwf[intlon_hamburg,intlat_hamburg][:,:,:],\
                            windarrs_ecmwf[intlon_paris,intlat_paris][:,:,:],\
                            'Question4ECMWF')
        
    print('Question 4 - ECMWF, lead times 0,3,7')
        
    # Plot wind speed histograms for lead times 0,3,7 for London, Paris, Hamburg (ECMWF)
    plt.PlotQuestion4LeadTime(windarrs_ecmwf[intlon_london,intlat_london][:,:,:],\
                              windarrs_ecmwf[intlon_hamburg,intlat_hamburg][:,:,:],\
                              windarrs_ecmwf[intlon_paris,intlat_paris][:,:,:])
        
    # ECMWF - calculate 98th percentiles for each lead time
    ninety_eighth_perc_london, ninety_eighth_perc_paris, \
        ninety_eighth_perc_hamburg = hc.ninety_eighth_percentile_by_lead_time(\
                          windarrs_ecmwf[intlon_london,intlat_london][:,:,:],\
                          windarrs_ecmwf[intlon_hamburg,intlat_hamburg][:,:,:],\
                          windarrs_ecmwf[intlon_paris,intlat_paris][:,:,:])
       
    # Plot three lines on one graph showing 98th percentile by lead time
    # for the three locations
    plt.PlotQuestion4b(ninety_eighth_perc_london, ninety_eighth_perc_paris, \
                       ninety_eighth_perc_hamburg)
        
    return windarrs_ecmwf, file_dates_ecmwf 

def Question5(alat, alon, windarrs, data_dates, wind_98s_all_months):
    """
    Plot ERA5 wind and ALP for three days centred on storm Gero (11th Jan 2005)
    """    
    # # Lothar (Paris)
    # dstart = dt.datetime(1999,12,25)
    # dend = dt.datetime(1999,12,27)    

    # # Kyrill - 18/01/2007
    # dstart = dt.datetime(2007,1,17)
    # dend = dt.datetime(2007,1,19)
    
    # # Klaus - 24/01/2009
    # dstart = dt.datetime(2009,1,23)
    # dend = dt.datetime(2009,1,25)
    
    # # Xynthia - 27/02/2010
    # dstart = dt.datetime(2010,2,26)
    # dend = dt.datetime(2010,2,28)
    
    # # Erwin - 27/02/2010
    # dstart = dt.datetime(2005,1,7)
    # dend = dt.datetime(2005,1, 9)
    
    # # Emma - 27/02/2010
    # dstart = dt.datetime(2008,2,28)
    # dend = dt.datetime(2008,3,1)
    
    # Gero
    dstart = dt.datetime(2005,1,10)
    dend = dt.datetime(2005,1, 12)

    windarrs_filtered, data_dates_filtered = \
            utl.filter_wind2d_by_date_range(windarrs, data_dates, dstart, dend)
    
    rgc.plot_storm(alon, alat, windarrs_filtered)
    
    # Calculate Accumulated Loss Potentials
    ALPs = utl.calculate_SSIs(windarrs_filtered, wind_98s_all_months)
    
    rgc.plot_onproj2d(alon, alat, ALPs,'ALP, Storm Gero, 10/01/05-12/01/05 (ERA5)',\
                      'rainbow', 'Question5ALP')
    
    utl.calculate_ALP_at_3locations(ALPs, alon, alat)
    
def Question6Part1(alat, alon, windarrs_ecmwf, file_dates_ecmwf, wind_98s_all_months_map):
    """
    Investigate signature of Storm Gero in ECMWF data
    Storm Lothar hit on 11/01/2005
    
    Using 01/12/04 file, calculate ensemble ALPs for lead time of 40 days
    Using 08/12/99 file, calculate ensemble ALPs for lead time of 33 days
    Using 15/12/99 file, calculate ensemble ALPs for lead time of 26 days
    Using 22/12/99 file, calculate ensemble ALPs for lead time of 19 days
    Using 29/12/99 file, calculate ensemble ALPs for lead time of 12 days
    
    Parameters
    ==========
    alat: nparray of latitudes
    alon: nparray of longitudes
    windarrs_ecmwf: 2d array of 3d arrays of wind speeds 
    file_dates_ecmwf: array of data dates corresponding to windarrs_ecmwf time series
    wind_98s_all_months_map: V98 figures for each gridbox ((with matrix rotated
                             90 degrees clockwise for correct cartopy plotting)
                                                           
    Returns
    =======
    None
    
    """

    ALPs = []
    titles = []

    wind_98s_all_months = np.transpose(wind_98s_all_months_map)
    storm_date = dt.datetime(2005,1,11)
    
    Question6Part1Calc(storm_date, 40, alon, alat, windarrs_ecmwf, \
                       file_dates_ecmwf,  wind_98s_all_months, titles, ALPs)
    
    Question6Part1Calc(storm_date, 33, alon, alat, windarrs_ecmwf, \
                       file_dates_ecmwf,  wind_98s_all_months, titles, ALPs)
    
    Question6Part1Calc(storm_date, 26, alon, alat, windarrs_ecmwf, \
                       file_dates_ecmwf,  wind_98s_all_months, titles, ALPs)
    
    Question6Part1Calc(storm_date, 19, alon, alat, windarrs_ecmwf, \
                       file_dates_ecmwf,  wind_98s_all_months, titles, ALPs)
    
    Question6Part1Calc(storm_date, 12, alon, alat, windarrs_ecmwf, \
                       file_dates_ecmwf,  wind_98s_all_months, titles, ALPs)
        
    plt.PlotQuestion6(ALPs, titles)
        
def Question6Part1Calc(storm_date, lead_time, alon, alat, windarrs_ecmwf, \
                   file_dates_ecmwf,  wind_98s_all_months, titles, ALPs):
    """
    Calculate ensemble ALPs (see Question6Part1)
    """
    
    titles.append(f'ALP dist, 10/1/05-12/1/05, LT {lead_time} days')  
    
    ALPs.append(hc.three_day_alp_distribution(dt.datetime(2005,1,11),\
       alon, alat, windarrs_ecmwf, file_dates_ecmwf, lead_time, wind_98s_all_months))
    
def Question6Part2(alat, alon, windarrs_ecmwf, wind_98s_all_months_map):
    """
    Calculate probability of storm stronger than Gero at London and Hamburg
    (ECMWF data)  

    Parameters
    ==========
    alat: nparray of latitudes
    alon: nparray of longitudes
    windarrs_ecmwf: 2d array of 3d arrays of wind speeds     
    wind_98s_all_months_map: V98 figures for each gridbox ((with matrix rotated
                             90 degrees clockwise for correct cartopy plotting)      
    """
    wind_98s_all_months = np.transpose(wind_98s_all_months_map)
    
    london_gero_alp = 0.01557
    hamburg_gero_alp = 0.00211

    # Fetch gridboxes for london and hamburg        
    intlon_london, intlat_london = rws.subset_field(alon, alat, \
                                          LONDON_LON, LONDON_LAT, iprint=0)
    intlon_hamburg, intlat_hamburg = rws.subset_field(alon, alat, \
                                        HAMBURG_LON, HAMBURG_LAT, iprint=0)
    
    london_gero_prob = hc.get_alp_probability(intlon_london, intlat_london, \
                     london_gero_alp, windarrs_ecmwf, wind_98s_all_months)
        
    hamburg_gero_prob = hc.get_alp_probability(intlon_hamburg, intlat_hamburg, \
                    hamburg_gero_alp, windarrs_ecmwf, wind_98s_all_months)
        
    print(f'London P(Gero): {london_gero_prob}')
    print(f'Hamburg P(Gero): {hamburg_gero_prob}')