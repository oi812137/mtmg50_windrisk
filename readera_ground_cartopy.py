"""
Template code for reading ERA5 data in netCDF format

Author: 2020, John Methven

CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
from netCDF4 import Dataset
import datetime
from datetime import date
from datetime import timedelta
import utilities as utl

def plot_onproj2d(alon,alat,field,title,cmapName,filename,vmin=None,vmax=None):

    '''
    Plot 2-D field on map using cartopy map projection.
    Input: longitude, latitude, time-index, infield, name of field
    Output: Plot of field
    '''  
    fig = plt.figure()
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.coastlines(resolution='50m', color='black', linewidth=1)
    ax.gridlines()
    plt.suptitle(title)
    nlevs = 20
        
    mycmap2 = plt.get_cmap(cmapName)    
    cs = plt.contourf(alon, alat, field, nlevs,transform=ccrs.PlateCarree(),\
                      cmap=mycmap2,vmin=vmin,vmax=vmax)
    
    plot_cities(ax,cmapName)    
    cbar = plt.colorbar(fraction=0.03, pad=0.04)
    
    plt.savefig('../figures/' + filename + '.jpg')
    plt.show()
    
def plot_cities(ax,cmapName):
    if cmapName =='rainbow':
        textcolor = 'w'
    else:
        textcolor = 'b'
    ax.plot(0.1076, 51.8072, '^w', markersize=7, transform=ccrs.Geodetic())
    ax.text(-4, 52, 'London', transform=ccrs.Geodetic(), color=textcolor)
    ax.plot(2.3522, 48.8566, '^w', markersize=7, transform=ccrs.Geodetic())
    ax.text(3, 48.8, 'Paris', transform=ccrs.Geodetic(), color=textcolor)
    ax.plot(9.9937, 53.5511, '^w', markersize=7, transform=ccrs.Geodetic())
    ax.text(10.2, 52, 'Hamburg', transform=ccrs.Geodetic(), color=textcolor)
 
def plot_storm(alon, alat, windarrs):
    '''
    Maps of wind of passing storm (three consecutive dates)
    
    Inputs
    ======
    alon: nparray of longitudes
    alat: nparray of latitudes
    windarrs: 1D nparray containing 3 nparrays (wind time series)
    title: Title of plot    
    '''  
   
    plot_onproj2d(alon, alat, utl.get_map_wind_by_index(windarrs,0), \
                 'Storm Gero (10/01/2005)', 'OrRd', \
                 'Question5StormGero10thJan', 0, 24)
    plot_onproj2d(alon, alat, utl.get_map_wind_by_index(windarrs,1), \
                 'Storm Gero (11/01/2005)', 'OrRd', \
                 'Question5StormGero11thJan', 0, 24)
    plot_onproj2d(alon, alat, utl.get_map_wind_by_index(windarrs,2), \
                 'Storm Gero (12/01/2005)', 'OrRd', \
                 'Question5StormGero12thJan', 0, 24)

def read_ground(fstem, fname):

    '''
    Read in the ground variable data from the netCDF file.
    Input: path and name of file to read.
    Output: 
    :longitude  - degrees
    :latitude   - degrees
    :time       - month number
    :gpheight   - geopotential height of the ground (above the geoid)
    :lsmask     - land-sea mask
    :fraclake   - fraction of grid box covered by lakes
    '''
    filename = str(fstem+fname)
    data = Dataset(filename, 'r')

    rtime = data.variables['time'][:]
    alon = data.variables['longitude'][:]
    alat = data.variables['latitude'][:]
    gp = data.variables['z'][:,:,:]
    lsmask = data.variables['lsm'][:,:,:]
    fraclake = data.variables['cl'][:,:,:]    
    data.close()
    #
    # Convert surface geopotential to geopotential height (m)
    #
    gpheight = gp/9.80665
    #
    # Time is in hours since 00UT, 1 Jan 1900.
    # Convert to timedelta format.
    #
    ftime = float(rtime)
    dtime = timedelta(hours=ftime)
    #
    # Note that you can add times in datetime and timedelta formats
    # which allows for leap years etc in time calculations.
    #
    startcal = datetime.datetime(1900, 1, 1)
    newcal = startcal+dtime
    # print(newcal)

    return alon, alat, newcal, gpheight, lsmask, fraclake
