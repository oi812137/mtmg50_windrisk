#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 12:29:38 2022

@author: 29812137

CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

LONDON_LON = -0.1248
LONDON_LAT = 51.5081

PARIS_LON = 2.3522
PARIS_LAT = 48.8566

HAMBURG_LON = 9.9937
HAMBURG_LAT = 53.5511

FPATH_DATA = '../data/ERA5/europe/europe_1halfx1half_ERA5_winds/'
FSTEM_DATA = 'eur_remap_bilinear_1halfx1half_ERA5_3hr_'

FSTEM_GROUND = '../data/ERA5/'
FNAME_GROUND = 'europe_ERA5ground.20180601.nc'

FPATH_NCEP = '../data/NCEP/'
FPATH_ECMWF = '../data/ECMWF/'
FSTEM_HC = 'eur_inst_energy_hc_'