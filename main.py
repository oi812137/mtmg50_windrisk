#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 12:32:54 2022

@author:  29812137

CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

import datetime as dt
import taskwrapper as t

def main():
    
    dstart = dt.datetime(1979, 1, 1)
    # dstart = dt.datetime(2010, 1, 1)
    dend = dt.datetime(2010, 2, 28)

    # Plot wind distribution, all months, full history at 3 locations (ERA5)
    t.Question1(dstart, dend, winterFilter=False, boxplotlimit=18, filestem='Question1AllMonths')
    
    # Plot wind distribution, winter months, dates with S2S coverage at 3 locations (ERA5)
    dstart = dt.datetime(1999, 12, 1)
    t.Question1(dstart, dend, winterFilter=True, boxplotlimit=14, filestem='Question1WinterMonths')
    
    # Plot 98th percentile, all months AND winter months, full history, 
    # across Europe (ERA5)
    alat, alon, windarrs_winter, data_dates_winter, wind_98s_all_months_map\
        , wind_98s_winter_months = t.Question2(dstart, dend, loadDataFromFile=True)

    # Plot accumulated loss potential in WINTER months across Europe (ERA5)
    # from 01/12/1999 - 28/02/2010 (period of S2S coverage)
    t.Question3(alat, alon, windarrs_winter, data_dates_winter, dstart, \
                dend, wind_98s_all_months_map)
    
    # Plot wind distribution in winter months at London, Paris & Hamburg (S2S)
    # WARNING - windarrs_ecmwf[0,0] is [lon,lat] = [-12,60] - TOP LEFT CORNER OF MAP
    windarrs_ecmwf, file_dates_ecmwf  = t.Question4(dstart, dend, loadDataFromFile=True)
    
    # Plot wind and ALP, storm Gero, 11th January 2005 (ERA5)
    t.Question5(alat, alon, windarrs_winter, data_dates_winter, wind_98s_winter_months)
    
    # WARNING - windarrs_ecmwf[0,0] is [lon,lat] = [-12,60] - TOP LEFT CORNER OF MAP
    
    # Calculate ALP over 40, 33, 26, 19, 12
    t.Question6Part1(alat, alon, windarrs_ecmwf, file_dates_ecmwf, wind_98s_all_months_map)
    
    
    t.Question6Part2(alat, alon, windarrs_ecmwf, wind_98s_all_months_map)
    
if __name__ == '__main__':
    main()