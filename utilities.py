#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 19:02:25 2022

@author: 29812137
"""

import numpy as np
import datetime as dt
from datetime import timedelta
import pandas as pnd
import era5_wind_series as rws
from init import *

def calculate_percentile(windarrs, percentile):
    """
    Calculate 98th percentile for each gridbox from wind time series passed in

    Parameters
    ----------
    windarrs : 2d array of wind series [lon,lat]
    percentile : percentile value to calculate (e.g. 98)

    Returns
    -------
    wind_98s_map : 98th percentile winds by location in configuration for plotting
    """
    
    wind_98s_map = np.empty(shape=(windarrs.shape[1],windarrs.shape[0]),dtype=float)    
    
    for i in range(windarrs.shape[0]):
        for j in range(windarrs.shape[1]):
            # Invert longitude and latitude for cartopy plotting            
            wind_98s_map[j,i]=np.percentile(windarrs[i,j], 98)
            
    return wind_98s_map

def filter_wind_by_dateindex(windarrs, date_index):
    """
    Return wind data for given date

    Parameters
    ----------
    windarrs : [lon,lat] nparray of 1d nparrays (wind data per date)
    date_index : index of date in 1d nparray within windarrs

    Returns
    -------
    wind : [lon,lat] nparray of float (wind speed for that date at each location)
    """

    # Invert longitude and latitude for cartopy plotting
    wind = np.empty(shape=(windarrs.shape[1],windarrs.shape[0]),dtype=float)
    
    for i in range(windarrs.shape[0]):
        for j in range(windarrs.shape[1]):
            # Invert longitude and latitude for cartopy plotting
            wind[j,i]=windarrs[i,j][date_index]
            
    return wind

def filter_winter(windarr, data_dates):
    """
    Filters wind speed data, returning time-series for only winter months
    
    Parameters
    ----------
    windarrs: 1d wind speed array indexed by date index
    date_index : nparray of dates correspoding to windarrs wind time series

    Returns
    -------
    nparray of (filtered) wind series    
    """
    
    windarr_wnt = []
    
    for i in range(len(data_dates)):
        if(data_dates[i].month in (12, 1, 2)):
            windarr_wnt.append(windarr[i])

    return np.array(windarr_wnt)

def filter_wind2d_by_date_range(windarrs, data_dates, dstart, dend):
    """
    Filters wind speed data on date range (dstart - dend)
    
    Inputs
    =======
    windarrs:   2d [lon,lat] nparray of nparrays (wind time series)
    data_dates: dates for wind time series
    dstart:     lower bound for date filter
    dend:       upper bound for date filter
    
    Returns
    =======
    windarrs_filtered:  2d [lon,lat] nparray of nparrays (wind time series) 
    """
        
    windarrs_shape = windarrs.shape
    windarrs_filtered = np.empty(shape=windarrs_shape, dtype=object)
    data_dates_filtered = []
    
    # Initialise lists
    for i in range(windarrs_shape[0]):
        for j in range(windarrs_shape[1]):
            windarrs_filtered[i,j] = []
    
    # Filter data
    for k in range(len(data_dates)):
        if((data_dates[k] >= dstart) and (data_dates[k] <= dend)):
            data_dates_filtered.append(data_dates[k])
            for i in range(windarrs_shape[0]):
                for j in range(windarrs_shape[1]):
                    windarrs_filtered[i,j].append(windarrs[i,j][k])                    
                 
    # Convert lists to nparrays
    for i in range(windarrs_shape[0]):
        for j in range(windarrs_shape[1]):
            windarrs_filtered[i,j] = np.array(windarrs_filtered[i,j])

    data_dates_filtered = np.array(data_dates_filtered)

    return windarrs_filtered, data_dates_filtered    

def filter_wind2d_winter(windarrs, data_dates):
    """
    Filters wind speed data, returning time-series for only winter months
    
    Inputs
    =======
    windarrs:   2d [lon,lat] nparray of nparrays (wind time series)
    data_dates: dates for wind time series
    
    Returns
    =======
    windarrs_winter: 2d [lon,lat] nparray of nparrays (wind time series) 
    """
        
    windarrs_shape = windarrs.shape
    windarrs_winter = np.empty(shape=windarrs_shape, dtype=object)
    data_dates_winter = []
    
    # Initialise lists
    for i in range(windarrs_shape[0]):
        for j in range(windarrs_shape[1]):
            windarrs_winter[i,j] = []
    
    # Filter data
    for k in range(len(data_dates)):
        if(data_dates[k].month in (12, 1, 2)):
            data_dates_winter.append(data_dates[k])
            for i in range(windarrs_shape[0]):
                for j in range(windarrs_shape[1]):
                    windarrs_winter[i,j].append(windarrs[i,j][k])                    
                 
    # Convert lists to nparrays
    for i in range(windarrs_shape[0]):
        for j in range(windarrs_shape[1]):
            windarrs_winter[i,j] = np.array(windarrs_winter[i,j])

    data_dates_winter = np.array(data_dates_winter)

    return windarrs_winter, data_dates_winter

def calculate_SSIs(windarrs, wind_98s_map):
    """
    Calculate SSI at each gridbox

    Parameters
    ----------
    windarrs:   2d [lon,lat] nparray of nparrays (wind time series)
    wind_98s_map : 98th percentile winds by location in configuration for plotting

    Returns
    -------
    nparray (2d) of SSIs

    """
    
    # SSI = (V/V98 -1)^3 for V>V98
    
    SSIs = np.zeros_like(wind_98s_map)
    
    for i in range(windarrs.shape[0]):
        for j in range(windarrs.shape[1]):
            windarr = windarrs[i,j]
            perc98 = wind_98s_map[j,i]
            for k in range(len(windarr)):
                if(windarr[k] > perc98):
                    SSIs[j,i] += (windarr[k]/perc98-1)**3

    return SSIs

def calculate_SSI(wind, wind_98):
    
    SSI = 0
    
    if wind > wind_98:
        SSI = (wind/wind_98-1)**3
        
    return SSI
        

def calculate_ALP_at_3locations(ALPs, alon, alat):
    """
    Fetch and print ALP at London, Paris, Hamburg from set of ALPs across Europe
    
    Parameters
    ----------
    ALPS:   2d [lon,lat] nparray of ALPs
    alon :  array of longitudes (corresponding to ALPs matrix)
    alat :  array of latitudes (corresponding to ALPs matrix)   

    Returns
    -------
    None
    
    """
    
    ALPs_trans = np.transpose(ALPs)
    
    # Find the indices of the grid box centred closest to the chosen location
    intlon_london, intlat_london = rws.subset_field(alon, alat, \
                                                    LONDON_LON, LONDON_LAT, iprint=0)
    intlon_hamburg, intlat_hamburg = rws.subset_field(alon, alat, \
                                                      HAMBURG_LON, HAMBURG_LAT, iprint=0)
    intlon_paris, intlat_paris = rws.subset_field(alon, alat, PARIS_LON, \
                                                  PARIS_LAT, iprint=0)
        
    ALPLondon = np.round(ALPs_trans[intlon_london,intlat_london], 5)
    ALPParis = np.round(ALPs_trans[intlon_paris,intlat_paris], 5)
    ALPHamburg = np.round(ALPs_trans[intlon_hamburg,intlat_hamburg], 5)
        
    ALPEurope = np.sum(ALPs_trans)
    
    print('Question 5')

    print(f'ALP at London: {ALPLondon}')
    print(f'ALP at Paris: {ALPParis}')
    print(f'ALP at Hamburg: {ALPHamburg}')
    print(f'ALP over Europe: {ALPEurope}')
   

def get_map_wind_by_index(windarrs,index):
    """
    Return wind data with given index for plotting on map
    
    Inputs
    ======
    windarrs = 2d array [lon,lat] containing nparrays of wind time series
    index = time index of required wind data
    
    Outputs
    =======
    Wind data for given time index for plotting on map
    
    """
    
    dataForPlotting = np.zeros(shape=(windarrs.shape[1], windarrs.shape[0]))
    
    for i in range(windarrs.shape[0]):
        for j in range(windarrs.shape[1]):
            dataForPlotting[j,i] = windarrs[i,j][index]

    return dataForPlotting

def get_winter_days(dstart, dend, include29thFeb):
    """
    Return nparray of date in Dec, Jan, Feb between dstart and dend    

    Parameters
    ----------
    dstart : Start date of interval
    dend :   End date of interval
    include29thFeb: Flag to indicate whether to include 29th Feb on leap years
    """
    
    file_dates = []
    dcur = dstart
    dendp = dend + timedelta(days=1)
    
    while(dcur<dendp):
        if((not include29thFeb) and dcur.month == 2 and dcur.day == 29):
            # Filter 29th Feb on leap years
            dcur = dcur + timedelta(days=1)
        elif not (dcur.month == 12 or dcur.month == 1 or dcur.month == 2):
            dcur = dt.datetime(dcur.year, dcur.month+1, 1)
        else:
            file_dates.append(dcur)
            dcur = dcur + timedelta(days=1)
            
    file_dates = np.array(file_dates)
    
    return file_dates

def get_ecmwf_days(dstart, dend):
    """
    Return nparray of date in Dec, Jan, Feb between dstart and dend  
    that correspond to dates available in ECMWF data files

    Parameters
    ----------
    dstart : Start date of interval
    dend :   End date of interval
    """
    
    file_dates = []
    
    # If dstart is January, start by processing all of January
    # If dstart is February, start by processing all of February
    # If dstart is March or later start by processing all of december
    
    dcur = dt.datetime(dstart.year, dstart.month, 1)
    if dend.month == 12:
        dendp = dt.datetime(dend.year, 12, 31)
    else:
        dendp = dt.datetime(dend.year, dend.month+1, 1) - timedelta(days=1)   
    
    endyear = dendp.year
    endmonth = dendp.month
    
    month = dcur.month
    year = dcur.year
    
    while dcur < dendp:
        if month == 1:
            advanceYear = False
            file_dates.append(dt.datetime(year, month, 4))
            file_dates.append(dt.datetime(year, month, 7))
            file_dates.append(dt.datetime(year, month, 11))
            file_dates.append(dt.datetime(year, month, 14))
            file_dates.append(dt.datetime(year, month, 18))
            file_dates.append(dt.datetime(year, month, 21))
            file_dates.append(dt.datetime(year, month, 25))
            file_dates.append(dt.datetime(year, month, 28))
        elif month == 2:
            advanceYear = False
            file_dates.append(dt.datetime(year, month, 1))
            file_dates.append(dt.datetime(year, month, 4))
            file_dates.append(dt.datetime(year, month, 8))
            file_dates.append(dt.datetime(year, month, 11))
            file_dates.append(dt.datetime(year, month, 15))
            file_dates.append(dt.datetime(year, month, 18))
            file_dates.append(dt.datetime(year, month, 22))
            file_dates.append(dt.datetime(year, month, 25))
            file_dates.append(dt.datetime(year, month, 28))
        else:
            advanceYear = True
            file_dates.append(dt.datetime(year, 12, 1))
            file_dates.append(dt.datetime(year, 12, 5))
            file_dates.append(dt.datetime(year, 12, 8))
            file_dates.append(dt.datetime(year, 12, 12))
            file_dates.append(dt.datetime(year, 12, 15))
            file_dates.append(dt.datetime(year, 12, 19))
            file_dates.append(dt.datetime(year, 12, 22))
            file_dates.append(dt.datetime(year, 12, 26))
            file_dates.append(dt.datetime(year, 12, 29))
            
        if advanceYear:
            dcur = dt.datetime(dcur.year+1,1,1)
        else:
            dcur = dt.datetime(dcur.year,month+1,1)
        
        month = dcur.month
        year = dcur.year
    
    # Clean up - remove dates outside daterange
    datesToRemove = []
    
    for i in range(len(file_dates)):
        if(file_dates[i] < dstart or file_dates[i] > dend):
            datesToRemove.append(file_dates[i])
            
    for i in range(len(datesToRemove)):
        file_dates.remove(datesToRemove[i])
            
    file_dates = np.array(file_dates)
    
    return file_dates