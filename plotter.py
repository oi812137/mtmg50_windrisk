#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 13:05:58 2022

@author:  29812137

CODE STRUCTURE

main.py: Single entry point for full execution
taskWrapper.py: Task-based high level function calls
era5_wind_series.py: Code for loading and processing ERA5 data
S2S_hincast.py: Code for loading and processing S2S hindcast data
init.py: variable initialisation
utilities.py: data manipulation toolkit
plotter.py: matplotlib plotting logic
cartopy.py: cartopy plotting logic
"""

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import scipy.stats as sts
import utilities as utl

def PlotQuestion1(windarr_london, windarr_paris, windarr_hamburg, timarr, \
                  title, boxplotlimit, filestem):    
    
    PlotWindspeedHistograms(windarr_london, windarr_paris, windarr_hamburg, title, filestem)
        
    PlotWindspeedBoxplots(windarr_london, windarr_paris, windarr_hamburg, title, \
                          boxplotlimit, filestem)
        
def PlotQuestion4NCEP(windarr_london, windarr_paris, windarr_hamburg, filestem):    
            
    PlotWindspeedHistograms(windarr_london, windarr_paris, windarr_hamburg, \
                            'Wind Speed, Dec 1990-Feb 2010, Winter Months (NCEP)', \
                                filestem)

def PlotQuestion4ECMWF(windarr_london, windarr_paris, windarr_hamburg, filestem):    
            
    PlotWindspeedHistograms(windarr_london, windarr_paris, windarr_hamburg, \
                            'Wind Speed, Dec 1990-Feb 2010, Winter Months (ECMWF)', \
                                filestem)
        
def PlotQuestion4LeadTime(windarr_london, windarr_paris, windarr_hamburg):
    
    PlotWindspeedHistogramsLeadTime(windarr_london, windarr_paris, \
                                    windarr_hamburg, 'ECMWF S2S Data')
        
def PlotQuestion4b(ninety_eighth_perc_london, ninety_eighth_perc_paris, \
                   ninety_eighth_perc_hamburg):
    
    # Plot three lines on one graph showing 98th percentile by lead time
    # for the three locations
    
    x = np.arange(len(ninety_eighth_perc_london))
    
    fig,ax=plt.subplots(1,1, figsize=(10,5))
    
    ax.plot(x, ninety_eighth_perc_london, label='London')
    ax.plot(x, ninety_eighth_perc_paris, label='Paris')
    ax.plot(x, ninety_eighth_perc_hamburg, label='Hamburg')
    
    ax.set_xlabel('Lead Time (days)')
    ax.set_ylabel('Wind Speed  m/s (98th percentile)')
   
    plt.suptitle('98th Pcntl Wind m/s, Dec 1999-Feb 2010, Winter Months (ECMWF)', fontsize=18)

    plt.legend()
    plt.tight_layout()
    plt.savefig('../figures/Question4b.jpg')
    plt.show()

def PlotQuestion6(ALPs, titles):
    
    fig,ax=plt.subplots(1,3, figsize=(15,5))
    
    bins = np.arange(0,4,0.5)
    
    for i in range(3):
        ax[i].hist(ALPs[i], bins, density=False)
        ax[i].set_xlabel('Ensemble ALP (Europe)')
        ax[i].set_ylabel('Frequency')
        ax[i].set_title(titles[i])
    
    plt.tight_layout()
    plt.savefig('../figures/Question6Gero1.jpg')
    plt.show()
    
    fig,ax=plt.subplots(1,2, figsize=(10,5))
    
    for i in range(2):
        ax[i].hist(ALPs[i+3], bins, density=False)
        ax[i].set_xlabel('Ensemble ALP (Europe)')
        ax[i].set_ylabel('Frequency')
        ax[i].set_title(titles[i+3])
    
    plt.tight_layout()
    plt.savefig('../figures/Question6Gero2.jpg')
    plt.show()
   
def PlotWindspeedHistograms(windarr_london, windarr_paris, windarr_hamburg, title, filestem):
    
    fig,ax=plt.subplots(1,3, figsize=(10,5))
    
    subplot_windspeed_histogram(ax[0], windarr_london, 'London')
    subplot_windspeed_histogram(ax[1], windarr_paris, 'Paris')
    subplot_windspeed_histogram(ax[2], windarr_hamburg, 'Hamburg')
    
    plt.suptitle(title, fontsize=18)
    plt.tight_layout()
    plt.savefig('../figures/' + filestem + 'hist.jpg')
    
    plt.show()
    
def PlotWindspeedBoxplots(windarr_london, windarr_paris, windarr_hamburg, \
                          title, boxplotlimit, filestem):
    
    fig,ax=plt.subplots(1,3, figsize=(10,5))
    
    subplot_windspeed_boxplot(ax[0], windarr_london, 'London', boxplotlimit)
    subplot_windspeed_boxplot(ax[1], windarr_paris, 'Paris', boxplotlimit)
    subplot_windspeed_boxplot(ax[2], windarr_hamburg, 'Hamburg', boxplotlimit)
    
    plt.suptitle(title, fontsize=18)
    plt.tight_layout()
    plt.savefig('../figures/' + filestem + 'box.jpg')
    
    plt.show()

def subplot_windspeed_histogram(subplot, windarr, title):

    Nbins = 80
    
    windarr_flat = np.ravel(windarr)
    maxwind = np.ceil(np.max(windarr_flat))
    
    subplot.hist(windarr_flat, Nbins, density=True)
    subplot.set_title(title, fontsize=16)
    subplot.set_xlabel('Wind Speed (m/s)')   
    subplot.set_ylabel('Probability')
    
    p_50 = np.percentile(windarr_flat, 50)
    p_75 = np.percentile(windarr_flat, 75)
    p_98 = np.percentile(windarr_flat, 98)
    
    print(f'{title}: 50th={round(p_50,2)}, 75th={round(p_75,2)}, 98th={round(p_98,2)}')
    
    subplot.axvline(p_50, color='orange', linestyle='dotted')
    subplot.axvline(p_75, color='orange', linestyle='dotted')
    subplot.axvline(p_98, color='orange', linestyle='dotted')
    
    # Fit a gamma function    
    x = np.linspace(0, maxwind, 80)
    fit_alpha, fit_loc, fit_scale = sts.gamma.fit(windarr_flat, floc=0)
    pdf_fitted = sts.gamma.pdf(x, fit_alpha, fit_loc, fit_scale)
    subplot.plot(x, pdf_fitted, color='r', label=f'$\Gamma$({round(fit_alpha,1)})')
    
    subplot.legend()
    
def subplot_windspeed_boxplot(subplot, windarr, title, boxplotlimit):

    Nbins = 80
    
    windarr_flat = np.ravel(windarr)
    maxwind = np.ceil(np.max(windarr_flat))
    
    subplot.boxplot(windarr_flat)
    
    subplot.set_title(title, fontsize=16)
    subplot.set_ylabel('Wind Speed (m/s)')   
    subplot.set_xticks([])
    subplot.set_yticks(np.arange(0, boxplotlimit+2, 2))

def PlotWindspeedHistogramsLeadTime(windarr_london, windarr_paris, windarr_hamburg, title):
    
    fig,ax=plt.subplots(3,3, figsize=(10,15))
    
    subplot_windspeed_histogram(ax[0,0], windarr_london[:,:,0], 'London, LT=0 days')
    subplot_windspeed_histogram(ax[0,1], windarr_london[:,:,3], 'London, LT=3 days')
    subplot_windspeed_histogram(ax[0,2], windarr_london[:,:,7], 'London, LT=7 days')
    subplot_windspeed_histogram(ax[1,0], windarr_paris[:,:,0], 'Paris, LT=0 days')
    subplot_windspeed_histogram(ax[1,1], windarr_paris[:,:,3], 'Paris, LT=3 days')
    subplot_windspeed_histogram(ax[1,2], windarr_paris[:,:,7], 'Paris, LT=7 days')
    subplot_windspeed_histogram(ax[2,0], windarr_hamburg[:,:,0], 'Hamburg, LT=0 days')
    subplot_windspeed_histogram(ax[2,1], windarr_hamburg[:,:,3], 'Hamburg, LT=3 days')
    subplot_windspeed_histogram(ax[2,2], windarr_hamburg[:,:,7], 'Hamburg, LT=7 days')
    
    plt.suptitle(title, fontsize=18)
    plt.tight_layout()
    plt.show()